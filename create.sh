#!/bin/bash
kubectl create -f deploy/crds/lab_v1_mongodbimage_crd.yaml
operator-sdk --image-builder=buildah build registry.gitlab.com/clintea/mongodb-rs-operator:v1
podman login -u${GITLAB_USER} -p${GITLAB_PASSWORD} registry.gitlab.com
podman push registry.gitlab.com/clintea/mongodb-rs-operator:v1
kubectl create -f deploy/service_account.yaml
kubectl create -f deploy/role.yaml
kubectl create -f deploy/role_binding.yaml
kubectl create -f deploy/operator.yaml
kubectl apply -f deploy/crds/lab_v1_mongodbimage_cr.yaml
